
dev: 
	go run ./cmd/main.go

build:
	rm -rf build
	mkdir build
	cp -R assets build/
	cp -R css build/
	cp -R dist build/
	cp -R public build/
	go build -v -o /tmp/ ./...
	mv /tmp/cmd ./build/app

clean:
	rm -rf build
